import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, Image } from 'react-native'
import { Button, ThemeProvider, Header, Icon, Tile, Divider, PricingCard } from 'react-native-elements';


class List extends Component {
    gotoProfile = () => {
        this.props.history.push('/profile')
    }
    gotoAdd = () => {
        this.props.history.push('/addpage')
    }
    render() {
        return (
            <View style={{ flex: 1 }}>


                <Header
                   
                    centerComponent={{ text: 'MY LIST', style: { color: '#fff', flex:1} }}

                >
                 {/* <TouchableOpacity style={styles.footerSidestyle}>
                        <Button title="<" />
                    </TouchableOpacity> */}
                </Header>

                <ScrollView>
                    <View style={styles.contendImage}>
                        <Image source={{ uri: 'https://i.pinimg.com/564x/79/37/db/7937db62251b6d5550a2b72c9f6f9533.jpg' }} style={styles.boxImage} />
                        <Image source={{ uri: 'https://i.pinimg.com/564x/49/1a/7c/491a7c90066d2a6836091db79acde465.jpg' }} style={styles.boxImage} />
                    </View>
                    <View style={styles.contendImage}>
                        <Image source={{ uri: 'https://i.pinimg.com/564x/79/37/db/7937db62251b6d5550a2b72c9f6f9533.jpg' }} style={styles.boxImage} />
                        <Image source={{ uri: 'https://i.pinimg.com/564x/79/37/db/7937db62251b6d5550a2b72c9f6f9533.jpg' }} style={styles.boxImage} />
                    </View>
                    <View style={styles.contendImage}>
                        <Image source={{ uri: 'https://i.pinimg.com/564x/79/37/db/7937db62251b6d5550a2b72c9f6f9533.jpg' }} style={styles.boxImage} />
                        <Image source={{ uri: 'https://i.pinimg.com/564x/79/37/db/7937db62251b6d5550a2b72c9f6f9533.jpg' }} style={styles.boxImage} />
                    </View>
                    <View style={styles.contendImage}>
                        <Image source={{ uri: 'https://i.pinimg.com/564x/79/37/db/7937db62251b6d5550a2b72c9f6f9533.jpg' }} style={styles.boxImage} />
                        <Image source={{ uri: 'https://i.pinimg.com/564x/79/37/db/7937db62251b6d5550a2b72c9f6f9533.jpg' }} style={styles.boxImage} />
                    </View>
                    <View style={styles.contendImage}>
                        <Image source={{ uri: 'https://i.pinimg.com/564x/79/37/db/7937db62251b6d5550a2b72c9f6f9533.jpg' }} style={styles.boxImage} />
                        <Image source={{ uri: 'https://i.pinimg.com/564x/79/37/db/7937db62251b6d5550a2b72c9f6f9533.jpg' }} style={styles.boxImage} />
                    </View>
                    <View style={styles.contendImage}>
                        <Image source={{ uri: 'https://i.pinimg.com/564x/79/37/db/7937db62251b6d5550a2b72c9f6f9533.jpg' }} style={styles.boxImage} />
                        <Image source={{ uri: 'https://i.pinimg.com/564x/79/37/db/7937db62251b6d5550a2b72c9f6f9533.jpg' }} style={styles.boxImage} />
                    </View>
                </ScrollView>
                <View style={[styles.row,]}>

                    <TouchableOpacity style={styles.footerSidestyle}>
                        <Button type="solid" title="L" />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ backgroundColor: 'black', width: '100%' }}>
                        <Button type="solid" title="ADD" onPress={this.gotoAdd}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.footerSidestyle} >
                        <Button  type="solid" title="P" onPress={this.gotoProfile}/>
                    </TouchableOpacity>

                </View>


            </View>
        );
    }
}

export default List

const styles = StyleSheet.create({
    header: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',


    },
    TextColor: {
        color: 'white',
    },
    sessionmiddle: {
        margin: 1,
        textAlign: 'center',
        backgroundColor: 'green',
        width: '80%',
        height: 50,
    }
    ,
    sessionSide: {
        margin: 1,
        textAlign: 'center',
        backgroundColor: 'green',
        width: '10%',
        height: 50,

    },
    sessionIcon: {
        margin: 1,
        textAlign: 'center',
        backgroundColor: 'green',
        width: '25%',
        height: 50,

    },
    contend: {
        // width: '100%',
        // height: '85%',
        backgroundColor: 'blue',
        flex: 1
    },
    row: {
        flexDirection: 'row',
        height: 50,
        width: 250,
        width: '75%'
    },
    footerSidestyle: {
        width: 55, height: 50, backgroundColor: 'blue'
    },
    boxImage: {
        width: 180, height: 180, margin: 5, marginEnd: 5, borderRadius: 4,
        borderWidth: 1,
        borderColor: 'black',
    },
    contendImage: {
        flex: 1, flexDirection: 'row', justifyContent: 'center',
    }

});