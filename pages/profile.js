import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, Image } from 'react-native'
import { Button, Header, SocialIcon } from 'react-native-elements';


class Profile extends Component {
    gotoList = () => {
        this.props.history.push('/list')
    }
    render() {
        return (
            <View style={{ flex: 1 }}>


                <Header

                    centerComponent={{ text: 'MY PROFILE', style: { color: '#fff', flex: 1 } }}

                >
                    <TouchableOpacity style={styles.headerSidestyle}>
                        <Button title="<" onPress={this.gotoList}/>
                    </TouchableOpacity>
                </Header>

                <ScrollView>

                    <SocialIcon
                        style={{ backgroundColor: '#0099ff' }}
                        title='UserName'
                        button
                    />
                    <SocialIcon
                        title='Frist name'
                        button
                        style={{ backgroundColor: '#0099ff' }}
                    />
                    <SocialIcon
                        title='Last name'
                        button
                        style={{ backgroundColor: '#0099ff' }}
                    />
                </ScrollView>

                <View >
                    <TouchableOpacity style={styles.footerSidestyle}>
                        <Button type="solid" title="EDIT" />
                    </TouchableOpacity>
                </View>


            </View>
        );
    }
}

export default Profile

const styles = StyleSheet.create({
    header: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',


    },
    TextColor: {
        color: 'white',
    },
    sessionmiddle: {
        margin: 1,
        textAlign: 'center',
        backgroundColor: 'green',
        width: '80%',
        height: 50,
    }
    ,
    sessionSide: {
        margin: 1,
        textAlign: 'center',
        backgroundColor: 'green',
        width: '10%',
        height: 50,

    },
    sessionIcon: {
        margin: 1,
        textAlign: 'center',
        backgroundColor: 'green',
        width: '25%',
        height: 50,

    },
    contend: {
        // width: '100%',
        // height: '85%',
        backgroundColor: 'blue',
        flex: 1
    },
    row: {
        flexDirection: 'row',
        height: 50,
        width: 250,
        width: '75%'
    },
    headerSidestyle: {
        width: 55, height: 50, backgroundColor: 'blue'
    },
    footerSidestyle: {
        width: 420, height: 50, backgroundColor: 'blue'
    },
    boxImage: {
        width: 180, height: 180, margin: 5, marginEnd: 5, borderRadius: 4,
        borderWidth: 1,
        borderColor: 'black',
    },
    contendImage: {
        flex: 1, flexDirection: 'row', justifyContent: 'center',
    }

});