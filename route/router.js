import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import List from '../pages/list'
import Profile from '../pages/profile'
import Add from '../pages/addpage'
import Login from '../pages/loginpage'
class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exact path="/list" component={List}/>
                    <Route exact path="/profile" component={Profile}/>
                    <Route exact path="/addpage" component={Add}/>
                    <Route exact path="/loginpage" component={Login}/>
                    <Redirect to ="/loginpage"/>
                </Switch>
            </NativeRouter>
        )
    }
}

export default Router 